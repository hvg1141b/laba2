/*
 * 2.	Минимум четырех чисел Написать функцию, которая вычисляет минимум из четырёх чисел.
 * Функция min(a,b,c,d) должна использовать (вызывать) функцию min(a,b).
 */

/**
 *
 * @author hvg1141b
 */
import java.util.Scanner;
public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        //проверка исключений
        try {
            System.out.println("Введите число a");
            a = sc.nextInt();
            System.out.println("Введите число b");
            b = sc.nextInt();
            System.out.println("Введите число c");
            c = sc.nextInt();
            System.out.println("Введите число d");
            d = sc.nextInt();
        } catch(Exception ex){
        System.out.println("При вводе произошла ошибка"+ex);
        }
        System.out.println(min2(a,b,c,d));

    }
    //функция нахождения минимального из 2
    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }
    //функция нахождения минимального из 4
    public static int min2(int a, int b,int c,int d) {
       int m1,m2,m;
       m1=min(a,b);
       m2=min(c,d);
       m=min(m1,m2);
       return m;
    }
}