/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Виктор
 */
import java.util.Scanner;
public class Task14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
        System.out.print("Введите № задания: ");
        int a = sc.nextInt(); // считывает целое число с потока ввода и сохраняем в переменную
 
        if (a==1){
          /*1.	Четные числа Используя цикл for вывести на экран чётные числа от 1 до 100 включительно. 
          * Через пробел либо с новой строки.
          */
            int i;
            for(i=2;i<=100;i+=2){
            //вывод
            System.out.print(i + " ");}
            }
    
        if (a==2){
          /* 2. Рисуем прямоугольник Ввести с клавиатуры два числа m и n. 
          * Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
          */
            int m = 0;
            int n = 0;
            System.out.println("Введите число m");        
            //проверка исключений
            try {
            m = sc.nextInt();
            } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
            }
            System.out.println("Введите число n");
            //проверка исключений
            try {
            n = sc.nextInt();
            } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
            }      
            for (int i = 1; i <= m; i++) {
                System.out.println();
            for (int j = 1; j <= n; j++) {
                System.out.print("8");
            }
            }
        }
      
        if (a==3){
        /* 3. Рисуем треугольник 
        * Используя цикл for вывести на экран прямоугольный треугольник из восьмёрок  
    `   * со сторонами 10 и 10.
        *
        * проход по главной диагональю и под ней
        */
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                //ВЫВОД НА ЭКРАН ВОСЬМЕРОК 
                if (j >= i) {
                    System.out.print("8");
                }
            }
            //ПЕРЕХОД НА НОВУЮ СТРОКУ
            System.out.println();
            } 
        }
     
        if (a==4){
        /* 4. Минимум двух чисел. Ввести с клавиатуры два числа, и 
        * вывести на экран минимальное из них (Поиск минимума выполняется в функции).
        */
            int m = 0;
            int n = 0;        
            //проверка исключений
            try {
            System.out.println("Введите число m");
            m = sc.nextInt();
            } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
            }
            //проверка исключений
            try {
            System.out.println("Введите число n");
            n = sc.nextInt();
            } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
            }
            System.out.println(min(m, n));
        }        
    }

    //функция нахождения минимального из 2
    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        } 
    }
}
