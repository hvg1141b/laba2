/*
 * 3.	Координатные четверти Ввести с клавиатуры два целых числа, которые будут координатами точки,
 * не лежащей на координатных осях OX и OY. Вывести на экран номер координатной четверти, в которой
 * находится данная точка.
 */

/**
 *
 * @author hvg1141b
 */
import java.util.Scanner;
public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = 0;
        int y = 0;
        //проверка исключений
        try {
            System.out.println("Введите кординату х");
            x = sc.nextInt();
            System.out.println("Введите кординату y");
            y = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода");
        }
        //проверка на условия нахождения на координатной плоскости
        if ((x > 0) && (y > 0)) {
            System.out.println("I четверть");
        }
        if ((x < 0) && (y > 0)) {
            System.out.println("II четверть");
        }
        if ((x < 0) && (y < 0)) {
            System.out.println("III четверть");
        }
        if ((x > 0) && (y < 0)) {
            System.out.println("IV четверть");
        }
        if ((x == 0) || (y == 0)) {
            System.out.println("На одной из осей координат");
        }
    }
}