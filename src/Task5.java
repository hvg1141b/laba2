/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Виктор
 */
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task5 {

    public static void main(String[] args) {
        String name1 = "";
        String name2 = "";
        Scanner sc = new Scanner(System.in);
        //проверка исключений
        try {
            System.out.println("Введите первое имя");
            name1 = sc.nextLine();
            System.out.println("Введите второе имя");
            name2 = sc.nextLine();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        //проверка на совпадение имен и длины имен
        if (haveOnly(name1) && haveOnly(name2)) {
            if (name1.equals(name2)) {
                System.out.println("Имена идентичны");
            } else if (name1.length() == name2.length()) {
                System.out.println("Длины имен равны");
            }
        }else System.out.println("Имена введины не правильно");

    }
//проверка принадлежности символов строки
    public static boolean haveOnly(String str) {
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(str);
        return m.matches();
    }
}